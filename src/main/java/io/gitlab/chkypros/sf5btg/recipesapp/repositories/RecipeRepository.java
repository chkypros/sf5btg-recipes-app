package io.gitlab.chkypros.sf5btg.recipesapp.repositories;

import io.gitlab.chkypros.sf5btg.recipesapp.domain.Recipe;
import org.springframework.data.repository.CrudRepository;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {
}
