package io.gitlab.chkypros.sf5btg.recipesapp.services;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface ImageService {
    void saveImageFile(long recipeId, MultipartFile imageFile);
}
