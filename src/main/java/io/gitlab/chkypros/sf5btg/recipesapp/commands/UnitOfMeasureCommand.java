package io.gitlab.chkypros.sf5btg.recipesapp.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UnitOfMeasureCommand {
    private Long id;
    private String name;
    private String descriptionSingular;
    private String descriptionPlural;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(descriptionSingular);

        if (!descriptionSingular.equals(descriptionPlural)) {
            builder.append('/').append(descriptionPlural);
        }

        return builder.toString();
    }
}
