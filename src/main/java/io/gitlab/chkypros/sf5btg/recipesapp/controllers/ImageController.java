package io.gitlab.chkypros.sf5btg.recipesapp.controllers;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.RecipeCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.services.ImageService;
import io.gitlab.chkypros.sf5btg.recipesapp.services.RecipeService;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Controller
public class ImageController {
    private final ImageService imageService;
    private final RecipeService recipeService;

    @Autowired
    public ImageController(ImageService imageService, RecipeService recipeService) {
        this.imageService = imageService;
        this.recipeService = recipeService;
    }

    @GetMapping("/recipe/{recipeId}/image")
    public String showImageUploadForm(@PathVariable String recipeId, Model model) {
        model.addAttribute("recipe", recipeService.findCommandById(Long.parseLong(recipeId)));

        return "/recipe/imageuploadform";
    }

    @PostMapping("/recipe/{recipeId}/image")
    public String uploadImageFile(@PathVariable String recipeId, @RequestParam("imagefile") MultipartFile multipartFile) {
        imageService.saveImageFile(Long.parseLong(recipeId), multipartFile);

        return "redirect:/recipe/" + recipeId + "/show";
    }

    @GetMapping("/recipe/{recipeId}/recipeimage")
    public void renderImage(@PathVariable String recipeId, HttpServletResponse response) throws IOException {
        RecipeCommand recipeCommand = recipeService.findCommandById(Long.parseLong(recipeId));
        byte[] imageBytes = new byte[recipeCommand.getImage().length];

        int i = 0;
        for (byte b : recipeCommand.getImage()) {
            imageBytes[i++] = b;
        }

        response.setContentType("image/jpeg");
        try (InputStream is = new ByteArrayInputStream(imageBytes)) {
            IOUtils.copy(is, response.getOutputStream());
        }
    }
}
