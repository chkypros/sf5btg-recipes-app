package io.gitlab.chkypros.sf5btg.recipesapp.services;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.RecipeCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Recipe;

import java.util.Set;

public interface RecipeService {
    Set<Recipe> getRecipes();

    Recipe findById(Long id);

    RecipeCommand saveRecipeCommand(RecipeCommand command);

    RecipeCommand findCommandById(Long id);

    void deleteById(Long id);
}
