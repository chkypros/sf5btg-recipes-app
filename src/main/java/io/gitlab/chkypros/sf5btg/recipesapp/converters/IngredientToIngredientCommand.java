package io.gitlab.chkypros.sf5btg.recipesapp.converters;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.IngredientCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Ingredient;
import lombok.Synchronized;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class IngredientToIngredientCommand implements Converter<Ingredient, IngredientCommand> {

    private final UnitOfMeasureToUnitOfMeasureCommand unitOfMeasureConverter;

    @Autowired
    public IngredientToIngredientCommand(UnitOfMeasureToUnitOfMeasureCommand unitOfMeasureConverter) {
        this.unitOfMeasureConverter = unitOfMeasureConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public IngredientCommand convert(Ingredient source) {
        if (source == null) {
            return null;
        }

        final IngredientCommand command = new IngredientCommand();
        BeanUtils.copyProperties(source, command);
        command.setUnitOfMeasure(unitOfMeasureConverter.convert(source.getUnitOfMeasure()));
        if (source.getRecipe() != null) {
            command.setRecipeId(source.getRecipe().getId());
        }

        return command;
    }
}
