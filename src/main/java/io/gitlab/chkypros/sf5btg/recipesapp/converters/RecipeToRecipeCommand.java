package io.gitlab.chkypros.sf5btg.recipesapp.converters;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.CategoryCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.commands.IngredientCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.commands.RecipeCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Recipe;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class RecipeToRecipeCommand implements Converter<Recipe, RecipeCommand> {
    private NotesToNotesCommand notesConverter;
    private CategoryToCategoryCommand categoryConverter;
    private IngredientToIngredientCommand ingredientConverter;

    @Autowired
    public RecipeToRecipeCommand(NotesToNotesCommand notesConverter, CategoryToCategoryCommand categoryConverter,
                                 IngredientToIngredientCommand ingredientConverter) {
        this.notesConverter = notesConverter;
        this.categoryConverter = categoryConverter;
        this.ingredientConverter = ingredientConverter;
    }

    @Override
    public RecipeCommand convert(Recipe source) {
        if (source == null) {
            return null;
        }

        final RecipeCommand command = new RecipeCommand();
        BeanUtils.copyProperties(source, command);
        command.setNotes(notesConverter.convert(source.getNotes()));

        if (source.getIngredients() != null && source.getIngredients().size() > 0) {
            Set<IngredientCommand> ingredients = new HashSet<>();
            source.getIngredients().forEach(ingredient -> ingredients.add(ingredientConverter.convert(ingredient)));
            command.setIngredients(ingredients);
        }

        if (source.getCategories() != null && source.getCategories().size() > 0) {
            Set<CategoryCommand> categories = new HashSet<>();
            source.getCategories().forEach(category -> categories.add(categoryConverter.convert(category)));
            command.setCategories(categories);
        }

        return command;
    }
}
