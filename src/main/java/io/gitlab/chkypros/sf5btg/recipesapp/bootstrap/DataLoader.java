package io.gitlab.chkypros.sf5btg.recipesapp.bootstrap;

import io.gitlab.chkypros.sf5btg.recipesapp.domain.*;
import io.gitlab.chkypros.sf5btg.recipesapp.repositories.CategoryRepository;
import io.gitlab.chkypros.sf5btg.recipesapp.repositories.RecipeRepository;
import io.gitlab.chkypros.sf5btg.recipesapp.repositories.UnitOfMeasureRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
public class DataLoader implements CommandLineRunner {
    private RecipeRepository recipeRepository;
    private CategoryRepository categoryRepository;
    private UnitOfMeasureRepository unitOfMeasureRepository;

    @Autowired
    public DataLoader(RecipeRepository recipeRepository, CategoryRepository categoryRepository,
                      UnitOfMeasureRepository unitOfMeasureRepository) {
        this.recipeRepository = recipeRepository;
        this.categoryRepository = categoryRepository;
        this.unitOfMeasureRepository = unitOfMeasureRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (recipeRepository.count() == 0) {
            loadData();
        }
    }

    private void loadData() {
        addPerfectGuacamoleRecipe();
        addSpicyGrilledChickenTacos();
    }

    private void addPerfectGuacamoleRecipe() {
        log.debug("Adding data for the Perfect Guacamole Recipe");
        Recipe recipe = new Recipe();

        Set<Category> categories = new HashSet<>();

        recipe.setDescription("Perfect Guacamole Recipe");
        recipe.setPrepTime(10);
        recipe.setCookTime(0);
        recipe.setServings(3);
        recipe.setDifficulty(Difficulty.EASY);
        recipe.setSource("https://www.simplyrecipes.com");
        recipe.setUrl("https://www.simplyrecipes.com/recipes/perfect_guacamole");

        categoryRepository.findByDescription("Dip").ifPresent(categories::add);
        categoryRepository.findByDescription("Mexican").ifPresent(categories::add);
        categoryRepository.findByDescription("Vegan").ifPresent(categories::add);
        categoryRepository.findByDescription("Avocado").ifPresent(categories::add);
        recipe.setCategories(categories);

        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(2), "ripe avocados",unitOfMeasureRepository.findByName("Quantity").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1d/2), "Kosher salt", unitOfMeasureRepository.findByName("Teaspoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1), "fresh lime juice or lemon juice", unitOfMeasureRepository.findByName("Tablespoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(2), "minced red onion or thinly sliced green onion", unitOfMeasureRepository.findByName("Tablespoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(2), "serrano chiles, stems and seeds removed, minced", unitOfMeasureRepository.findByName("Quantity").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(2), "cilantro (leaves and tender stems), finely chopped", unitOfMeasureRepository.findByName("Tablespoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1), "freshly grated black pepper", unitOfMeasureRepository.findByName("Dash").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1d/2), "ripe tomato, seeds and pulp removed, chopped", unitOfMeasureRepository.findByName("Quantity").orElse(null)));

        recipe.setDirections("1 Cut avocado, remove flesh: Cut the avocados in half. Remove seed. " +
                "Score the inside of the avocado with a blunt knife and scoop out the flesh with a spoon. Place in a bowl.\n\n" +
                "2 Mash with a fork: Using a fork, roughly mash the avocado. (Don't overdo it! The guacamole should be a little chunky.)\n\n" +
                "3 Add salt, lime juice, and the rest: Sprinkle with salt and lime (or lemon) juice. " +
                "The acid in the lime juice will provide some balance to the richness of the avocado and will help delay the avocados from turning brown.\n" +
                "Add the chopped onion, cilantro, black pepper, and chiles. Chili peppers vary individually in their hotness. " +
                "So, start with a half of one chili pepper and add to the guacamole to your desired degree of hotness.\n" +
                "Remember that much of this is done to taste because of the variability in the fresh ingredients. Start with this recipe and adjust to your taste.\n\n" +
                "4 Cover with plastic and chill to store: Place plastic wrap on the surface of the guacamole cover it and to prevent air reaching it. " +
                "(The oxygen in the air causes oxidation which will turn the guacamole brown.) Refrigerate until ready to serve.\n" +
                "Chilling tomatoes hurts their flavor, so if you want to add chopped tomato to your guacamole, add it just before serving.");

        Notes notes = new Notes();
        notes.setRecipeNotes("Variations\n" +
                "\n" +
                "For a very quick guacamole just take a 1/4 cup of salsa and mix it in with your mashed avocados.\n" +
                "\n" +
                "Feel free to experiment! " +
                "One classic Mexican guacamole has pomegranate seeds and chunks of peaches in it (a Diana Kennedy favorite). " +
                "Try guacamole with added pineapple, mango, or strawberries.\n" +
                "\n" +
                "The simplest version of guacamole is just mashed avocados with salt. " +
                "Don't let the lack of availability of other ingredients stop you from making guacamole.\n" +
                "\n" +
                "To extend a limited supply of avocados, add either sour cream or cottage cheese to your guacamole dip. " +
                "Purists may be horrified, but so what? It tastes great.");
        recipe.setNotes(notes);

        recipeRepository.save(recipe);
    }

    private void addSpicyGrilledChickenTacos() {
        log.debug("Adding data for the Spicy Grilled Chicken Tacos Recipe");
        Recipe recipe = new Recipe();

        Set<Category> categories = new HashSet<>();

        recipe.setDescription("Spicy Grilled Chicken Tacos Recipe");
        recipe.setPrepTime(20);
        recipe.setCookTime(15);
        recipe.setServings(5);
        recipe.setDifficulty(Difficulty.MODERATE);
        recipe.setSource("https://www.simplyrecipes.com");
        recipe.setUrl("https://www.simplyrecipes.com/recipes/spicy_grilled_chicken_tacos/");

        categoryRepository.findByDescription("Dinner").ifPresent(categories::add);
        categoryRepository.findByDescription("Grill").ifPresent(categories::add);
        categoryRepository.findByDescription("Quick and Easy").ifPresent(categories::add);
        categoryRepository.findByDescription("Chicken").ifPresent(categories::add);
        recipe.setCategories(categories);

        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(2), "ancho chili powder", unitOfMeasureRepository.findByName("Tablespoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1), "dried oregano", unitOfMeasureRepository.findByName("Teaspoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1), "dried cumin", unitOfMeasureRepository.findByName("Teaspoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1), "sugar", unitOfMeasureRepository.findByName("Teaspoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1d/2), "salt", unitOfMeasureRepository.findByName("Teaspoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1), "garlic, finely chopped", unitOfMeasureRepository.findByName("Clove").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1), "finely grated orange zest", unitOfMeasureRepository.findByName("Tablespoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(3), "fresh-squeezed orange juice", unitOfMeasureRepository.findByName("Tablespoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(2), "olive oil", unitOfMeasureRepository.findByName("Tablespoon").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(5), "skinless, boneless chicken thighs (1 1/4 pounds)", unitOfMeasureRepository.findByName("Quantity").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(8), "small corn tortillas", unitOfMeasureRepository.findByName("Quantity").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(3), "packed baby arugula (3 ounces)", unitOfMeasureRepository.findByName("Cup").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(2), "medium ripe avocados, sliced", unitOfMeasureRepository.findByName("Quantity").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(4), "radishes, thinly sliced", unitOfMeasureRepository.findByName("Quantity").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1d/2), "cherry tomatoes, halved", unitOfMeasureRepository.findByName("Pint").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1d/4), "red onion, thinly sliced", unitOfMeasureRepository.findByName("Quantity").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1), "roughly chopped cilantro", unitOfMeasureRepository.findByName("Dash").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1d/2), "sour cream thinned with 1/4 cup milk", unitOfMeasureRepository.findByName("Cup").orElse(null)));
        recipe.addIngredient(new Ingredient(BigDecimal.valueOf(1), "lime, cut into wedges", unitOfMeasureRepository.findByName("Quantity").orElse(null)));

        recipe.setDirections("1 Prepare a gas or charcoal grill for medium-high, direct heat.\n" +
                "\n" +
                "2 Make the marinade and coat the chicken: " +
                "In a large bowl, stir together the chili powder, oregano, cumin, sugar, salt, garlic and orange zest. " +
                "Stir in the orange juice and olive oil to make a loose paste. " +
                "Add the chicken to the bowl and toss to coat all over.\n" +
                "Set aside to marinate while the grill heats and you prepare the rest of the toppings.\n\n" +
                "3 Grill the chicken: Grill the chicken for 3 to 4 minutes per side, " +
                "or until a thermometer inserted into the thickest part of the meat registers 165F. " +
                "Transfer to a plate and rest for 5 minutes.\n\n" +
                "4 Warm the tortillas: Place each tortilla on the grill or on a hot, dry skillet over medium-high heat. " +
                "As soon as you see pockets of the air start to puff up in the tortilla, " +
                "turn it with tongs and heat for a few seconds on the other side.\n" +
                "Wrap warmed tortillas in a tea towel to keep them warm until serving.\n\n" +
                "5 Assemble the tacos: Slice the chicken into strips. On each tortilla, place a small handful of arugula. " +
                "Top with chicken slices, sliced avocado, radishes, tomatoes, and onion slices. " +
                "Drizzle with the thinned sour cream. Serve with lime wedges.");

        Notes notes = new Notes();
        notes.setRecipeNotes("");
        recipe.setNotes(notes);

        recipeRepository.save(recipe);
    }
}
