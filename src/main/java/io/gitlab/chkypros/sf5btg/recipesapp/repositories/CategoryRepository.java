package io.gitlab.chkypros.sf5btg.recipesapp.repositories;

import io.gitlab.chkypros.sf5btg.recipesapp.domain.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    Optional<Category> findByDescription(String description);
}
