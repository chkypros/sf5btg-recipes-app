package io.gitlab.chkypros.sf5btg.recipesapp.domain;

public enum Difficulty {
    EASY, MODERATE, SOMEWHAT_HARD, HARD;

    private static final String DELIMITER = "_";

    @Override
    public String toString() {
        StringBuilder descriptionSB = new StringBuilder();

        for (String descriptionPart :
            name().split(DELIMITER)) {
            descriptionSB.append(toStartCaseWord(descriptionPart)).append(" ");
        }

        return descriptionSB.toString().trim();
    }

    private String toStartCaseWord(String word) {
        if (word.length() < 2) {
            return word.toUpperCase();
        } else {
            return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
        }
    }
}
