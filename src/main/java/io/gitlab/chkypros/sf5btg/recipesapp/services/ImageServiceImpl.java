package io.gitlab.chkypros.sf5btg.recipesapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import io.gitlab.chkypros.sf5btg.recipesapp.domain.Recipe;
import io.gitlab.chkypros.sf5btg.recipesapp.repositories.RecipeRepository;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
@Service
public class ImageServiceImpl implements ImageService {
    private RecipeRepository recipeRepository;

    @Autowired
    public ImageServiceImpl(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public void saveImageFile(long recipeId, MultipartFile imageFile) {
        log.debug("Received file {} for recipe {}", imageFile.getName(), recipeId);

        Recipe recipe = recipeRepository.findById(recipeId).get();

        try {
            Byte[] byteObjects = new Byte[imageFile.getBytes().length];

            int i = 0;
            for (byte b : imageFile.getBytes()) {
                byteObjects[i++] = b;
            }

            recipe.setImage(byteObjects);

            recipeRepository.save(recipe);
        } catch (IOException e) {
            // TODO Handle better
            log.error("Error occurred", e);
            e.printStackTrace();
        }
    }
}
