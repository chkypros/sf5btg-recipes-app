package io.gitlab.chkypros.sf5btg.recipesapp.domain;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Data
@EqualsAndHashCode(exclude = {"recipe"})
@Entity
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;
    private BigDecimal amount;

    @OneToOne(fetch = FetchType.EAGER)
    private UnitOfMeasure unitOfMeasure;

    @ManyToOne
    private Recipe recipe;

    public Ingredient() {}

    public Ingredient(BigDecimal amount, String description, UnitOfMeasure unitOfMeasure) {
        this.amount = amount;
        this.description = description;
        this.unitOfMeasure = unitOfMeasure;
    }

    public Ingredient(BigDecimal amount, String description, UnitOfMeasure unitOfMeasure, Recipe recipe) {
        this.description = description;
        this.amount = amount;
        this.unitOfMeasure = unitOfMeasure;
        this.recipe = recipe;
    }

    @Override
    public String toString() {
        StringBuilder uomBuilder = new StringBuilder();
        uomBuilder.append((amount.compareTo(BigDecimal.ONE) <= 0)
                ? unitOfMeasure.getDescriptionSingular()
                : unitOfMeasure.getDescriptionPlural());

        if (unitOfMeasure.getName().compareTo("Quantity") != 0) {
            uomBuilder.append(" of ");
        }

        return String.format("%.2f %s %s",amount, uomBuilder.toString(), description);
    }
}
