package io.gitlab.chkypros.sf5btg.recipesapp.repositories;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.chkypros.sf5btg.recipesapp.domain.Ingredient;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {
    @Override
    void deleteById(Long aLong);
}
