package io.gitlab.chkypros.sf5btg.recipesapp.converters;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.RecipeCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Category;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Ingredient;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Recipe;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class RecipeCommandToRecipe implements Converter<RecipeCommand, Recipe> {
    private NotesCommandToNotes notesConverter;
    private CategoryCommandToCategory categoryConverter;
    private IngredientCommandToIngredient ingredientConverter;

    @Autowired
    public RecipeCommandToRecipe(NotesCommandToNotes notesConverter, CategoryCommandToCategory categoryConverter,
                                 IngredientCommandToIngredient ingredientConverter) {
        this.notesConverter = notesConverter;
        this.categoryConverter = categoryConverter;
        this.ingredientConverter = ingredientConverter;
    }

    @Override
    public Recipe convert(RecipeCommand source) {
        if (source == null) {
            return null;
        }

        final Recipe recipe = new Recipe();
        BeanUtils.copyProperties(source, recipe);
        recipe.setNotes(notesConverter.convert(source.getNotes()));

        if (source.getIngredients() != null && source.getIngredients().size() > 0) {
            Set<Ingredient> ingredients = new HashSet<>();
            source.getIngredients().forEach(ingredientCommand -> ingredients.add(ingredientConverter.convert(ingredientCommand)));
            recipe.setIngredients(ingredients);
        }

        if (source.getCategories() != null && source.getCategories().size() > 0) {
            Set<Category> categories = new HashSet<>();
            source.getCategories().forEach(categoryCommand -> categories.add(categoryConverter.convert(categoryCommand)));
            recipe.setCategories(categories);
        }

        return recipe;
    }
}
