package io.gitlab.chkypros.sf5btg.recipesapp.converters;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.NotesCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Notes;
import lombok.Synchronized;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class NotesCommandToNotes implements Converter<NotesCommand, Notes> {
    @Synchronized
    @Nullable
    @Override
    public Notes convert(NotesCommand source) {
        if (source == null) {
            return null;
        }

        final Notes notes = new Notes();
        BeanUtils.copyProperties(source, notes);
        return notes;
    }}
