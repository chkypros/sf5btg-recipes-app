package io.gitlab.chkypros.sf5btg.recipesapp.repositories;

import io.gitlab.chkypros.sf5btg.recipesapp.domain.UnitOfMeasure;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UnitOfMeasureRepository extends CrudRepository<UnitOfMeasure, Long> {
    Optional<UnitOfMeasure> findByName(String name);

    Optional<UnitOfMeasure>  findByDescriptionSingular(String description);

    Optional<UnitOfMeasure>  findByDescriptionPlural(String description);
}
