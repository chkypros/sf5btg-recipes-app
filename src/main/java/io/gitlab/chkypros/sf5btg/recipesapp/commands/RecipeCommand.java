package io.gitlab.chkypros.sf5btg.recipesapp.commands;

import io.gitlab.chkypros.sf5btg.recipesapp.domain.Difficulty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class RecipeCommand {
    private Long id;
    private String  description;
    private Integer prepTime;
    private Integer cookTime;
    private Integer servings;
    private String  source;
    private String  url;
    private String  directions;
    private Difficulty difficulty;
    private Byte[] image;
    private Set<IngredientCommand> ingredients = new HashSet<>();
    private Set<CategoryCommand> categories = new HashSet<>();
    private NotesCommand notes;
}
