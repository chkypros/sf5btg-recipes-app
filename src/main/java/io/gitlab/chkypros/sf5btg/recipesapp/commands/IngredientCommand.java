package io.gitlab.chkypros.sf5btg.recipesapp.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class IngredientCommand {
    private Long id;
    private Long recipeId;
    private String description;
    private BigDecimal amount;
    private UnitOfMeasureCommand unitOfMeasure;

    @Override
    public String toString() {
        StringBuilder uomBuilder = new StringBuilder();
        uomBuilder.append((amount.compareTo(BigDecimal.ONE) <= 0)
                ? unitOfMeasure.getDescriptionSingular()
                : unitOfMeasure.getDescriptionPlural());

        if (unitOfMeasure.getName().compareTo("Quantity") != 0) {
            uomBuilder.append(" of ");
        }

        return String.format("%.2f %s %s",amount, uomBuilder.toString(), description);
    }
}
