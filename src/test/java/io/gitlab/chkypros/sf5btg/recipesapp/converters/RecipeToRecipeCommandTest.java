package io.gitlab.chkypros.sf5btg.recipesapp.converters;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.RecipeCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.*;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class RecipeToRecipeCommandTest {

    private static final Long ID_VALUE = 1L;
    private static final Integer COOK_TIME = 10;
    private static final Integer PREP_TIME = 5;
    private static final String DESCRIPTION = "Description";
    private static final Difficulty DIFFICULTY = Difficulty.EASY;
    private static final String DIRECTIONS = "Directions";
    private static final Integer SERVINGS = 2;
    private static final String SOURCE = "Source";
    private static final String URL = "www.example.com";
    private static final Long CATEGORY_ID_1 = 1L;
    private static final Long CATEGORY_ID_2 = 2L;
    private static final Long INGREDIENT_ID_1 = 3L;
    private static final Long INGREDIENT_ID_2 = 4L;
    private static final Long NOTES_ID = 5L;

    private RecipeToRecipeCommand converter;

    @Before
    public void setUp() {
        converter = new RecipeToRecipeCommand(new NotesToNotesCommand(),
                    new CategoryToCategoryCommand(),
                    new IngredientToIngredientCommand(new UnitOfMeasureToUnitOfMeasureCommand()));
    }

    @Test
    public void convert_nullParameter() {
        assertNull(converter.convert(null));
    }

    @Test
    public void convert_emptyObject() {
        assertNotNull(converter.convert(new Recipe()));
    }

    @Test
    public void convert() {
        // Given
        Recipe recipe1 = new Recipe();
        recipe1.setId(ID_VALUE);
        recipe1.setCookTime(COOK_TIME);
        recipe1.setPrepTime(PREP_TIME);
        recipe1.setDescription(DESCRIPTION);
        recipe1.setDifficulty(DIFFICULTY);
        recipe1.setDirections(DIRECTIONS);
        recipe1.setServings(SERVINGS);
        recipe1.setSource(SOURCE);
        recipe1.setUrl(URL);

        Category category1 = new Category();
        category1.setId(CATEGORY_ID_1);
        Category category2 = new Category();
        category1.setId(CATEGORY_ID_2);
        Set<Category> categories = new HashSet<>();
        categories.add(category1);
        categories.add(category2);
        recipe1.setCategories(categories);

        Set<Ingredient> ingredients = new HashSet<>();
        Ingredient ingredient1 = new Ingredient();
        ingredient1.setId(INGREDIENT_ID_1);
        Ingredient ingredient2 = new Ingredient();
        ingredient1.setId(INGREDIENT_ID_2);
        ingredients.add(ingredient1);
        ingredients.add(ingredient2);
        recipe1.setIngredients(ingredients);

        Notes notes = new Notes();
        notes.setId(NOTES_ID);
        recipe1.setNotes(notes);

        // When
        RecipeCommand command = converter.convert(recipe1);

        // Then
        assertNotNull(command);
        assertEquals(ID_VALUE, command.getId());
        assertEquals(COOK_TIME, command.getCookTime());
        assertEquals(PREP_TIME, command.getPrepTime());
        assertEquals(DESCRIPTION, command.getDescription());
        assertEquals(DIFFICULTY, command.getDifficulty());
        assertEquals(SERVINGS, command.getServings());
        assertEquals(SOURCE, command.getSource());
        assertEquals(URL, command.getUrl());
        assertEquals(NOTES_ID, command.getNotes().getId());
        assertEquals(categories.size(), command.getCategories().size());
        assertEquals(ingredients.size(), command.getIngredients().size());
    }
}
