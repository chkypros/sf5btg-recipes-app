package io.gitlab.chkypros.sf5btg.recipesapp.converters;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.CategoryCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.commands.IngredientCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.commands.NotesCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.commands.RecipeCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Difficulty;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Recipe;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class RecipeCommandToRecipeTest {

    private static final Long ID_VALUE = 1L;
    private static final Integer COOK_TIME = 10;
    private static final Integer PREP_TIME = 5;
    private static final String DESCRIPTION = "Description";
    private static final Difficulty DIFFICULTY = Difficulty.EASY;
    private static final String DIRECTIONS = "Directions";
    private static final Integer SERVINGS = 2;
    private static final String SOURCE = "Source";
    private static final String URL = "www.example.com";
    private static final Long CATEGORY_ID_1 = 1L;
    private static final Long CATEGORY_ID_2 = 2L;
    private static final Long INGREDIENT_ID_1 = 3L;
    private static final Long INGREDIENT_ID_2 = 4L;
    private static final Long NOTES_ID = 5L;

    private RecipeCommandToRecipe converter;

    @Before
    public void setUp() {
        converter = new RecipeCommandToRecipe(new NotesCommandToNotes(),
                                              new CategoryCommandToCategory(),
                                              new IngredientCommandToIngredient(new UnitOfMeasureCommandToUnitOfMeasure()));
    }

    @Test
    public void convert_nullParameter() {
        assertNull(converter.convert(null));
    }

    @Test
    public void convert_emptyObject() {
        assertNotNull(converter.convert(new RecipeCommand()));
    }

    @Test
    public void convert() {
        // Given
        RecipeCommand command = new RecipeCommand();
        command.setId(ID_VALUE);
        command.setCookTime(COOK_TIME);
        command.setPrepTime(PREP_TIME);
        command.setDescription(DESCRIPTION);
        command.setDifficulty(DIFFICULTY);
        command.setDirections(DIRECTIONS);
        command.setServings(SERVINGS);
        command.setSource(SOURCE);
        command.setUrl(URL);

        CategoryCommand category1 = new CategoryCommand();
        category1.setId(CATEGORY_ID_1);
        CategoryCommand category2 = new CategoryCommand();
        category1.setId(CATEGORY_ID_2);
        Set<CategoryCommand> categories = new HashSet<>();
        categories.add(category1);
        categories.add(category2);
        command.setCategories(categories);

        Set<IngredientCommand> ingredients = new HashSet<>();
        IngredientCommand ingredient1 = new IngredientCommand();
        ingredient1.setId(INGREDIENT_ID_1);
        IngredientCommand ingredient2 = new IngredientCommand();
        ingredient1.setId(INGREDIENT_ID_2);
        ingredients.add(ingredient1);
        ingredients.add(ingredient2);
        command.setIngredients(ingredients);

        NotesCommand notes = new NotesCommand();
        notes.setId(NOTES_ID);
        command.setNotes(notes);

        // When
        Recipe recipe = converter.convert(command);

        // Then
        assertNotNull(recipe);
        assertEquals(ID_VALUE, recipe.getId());
        assertEquals(COOK_TIME, recipe.getCookTime());
        assertEquals(PREP_TIME, recipe.getPrepTime());
        assertEquals(DESCRIPTION, recipe.getDescription());
        assertEquals(DIFFICULTY, recipe.getDifficulty());
        assertEquals(SERVINGS, recipe.getServings());
        assertEquals(SOURCE, recipe.getSource());
        assertEquals(URL, recipe.getUrl());
        assertEquals(NOTES_ID, recipe.getNotes().getId());
        assertEquals(categories.size(), recipe.getCategories().size());
        assertEquals(ingredients.size(), recipe.getIngredients().size());
    }
}
