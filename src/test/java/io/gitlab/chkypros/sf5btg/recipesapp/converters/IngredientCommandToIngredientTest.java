package io.gitlab.chkypros.sf5btg.recipesapp.converters;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.IngredientCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.commands.UnitOfMeasureCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Ingredient;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class IngredientCommandToIngredientTest {
    private static final Long ID_VALUE = 1L;
    private static final BigDecimal AMOUNT = BigDecimal.ONE;
    private static final String DESCRIPTION = "Cheeseburger";
    private static final Long UOM_ID = 2L;

    private IngredientCommandToIngredient converter;

    @Before
    public void setUp() {
        converter = new IngredientCommandToIngredient(new UnitOfMeasureCommandToUnitOfMeasure());
    }

    @Test
    public void convert_nullParameter() {
        assertNull(converter.convert(null));
    }

    @Test
    public void convert_emptyObject() {
        assertNotNull(converter.convert(new IngredientCommand()));
    }

    @Test
    public void convert() {
        // Given
        IngredientCommand command = new IngredientCommand();
        command.setId(ID_VALUE);
        command.setAmount(AMOUNT);
        command.setDescription(DESCRIPTION);
        UnitOfMeasureCommand unitOfMeasure = new UnitOfMeasureCommand();
        unitOfMeasure.setId(UOM_ID);
        command.setUnitOfMeasure(unitOfMeasure);

        // When
        Ingredient ingredient = converter.convert(command);

        // Then
        assertNotNull(ingredient);
        assertEquals(ID_VALUE, ingredient.getId());
        assertEquals(DESCRIPTION, ingredient.getDescription());
        assertEquals(AMOUNT, ingredient.getAmount());
        assertEquals(UOM_ID, ingredient.getUnitOfMeasure().getId());
    }

    @Test
    public void convert_nullUOM() {
        // Given
        IngredientCommand command = new IngredientCommand();
        command.setId(ID_VALUE);
        command.setAmount(AMOUNT);
        command.setDescription(DESCRIPTION);

        // When
        Ingredient ingredient = converter.convert(command);

        // Then
        assertNotNull(ingredient);
        assertNull(ingredient.getUnitOfMeasure());
        assertEquals(ID_VALUE, ingredient.getId());
        assertEquals(DESCRIPTION, ingredient.getDescription());
        assertEquals(AMOUNT, ingredient.getAmount());
    }
}