package io.gitlab.chkypros.sf5btg.recipesapp.converters;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.UnitOfMeasureCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.UnitOfMeasure;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UnitOfMeasureCommandToUnitOfMeasureTest {
    private static final Long LONG_VALUE = 1L;
    private static final String NAME = "name";
    private static final String DESCRIPTION_SINGULAR = "description-singular";
    private static final String DESCRIPTION_PLURAL = "description-plural";

    private UnitOfMeasureCommandToUnitOfMeasure converter;

    @Before
    public void setUp() {
        converter = new UnitOfMeasureCommandToUnitOfMeasure();
    }

    @Test
    public void convert_nullParameter() {
        assertNull(converter.convert(null));
    }

    @Test
    public void convert_emptyObject() {
        assertNotNull(converter.convert(new UnitOfMeasureCommand()));
    }

    @Test
    public void convert() {
        // given
        UnitOfMeasureCommand command = new UnitOfMeasureCommand();
        command.setId(LONG_VALUE);
        command.setName(NAME);
        command.setDescriptionSingular(DESCRIPTION_SINGULAR);
        command.setDescriptionPlural(DESCRIPTION_PLURAL);

        // when
        UnitOfMeasure unitOfMeasure = converter.convert(command);

        // then
        assertNotNull(unitOfMeasure);
        assertEquals(LONG_VALUE, unitOfMeasure.getId());
        assertEquals(NAME, unitOfMeasure.getName());
        assertEquals(DESCRIPTION_SINGULAR, unitOfMeasure.getDescriptionSingular());
        assertEquals(DESCRIPTION_PLURAL, unitOfMeasure.getDescriptionPlural());
    }
}
