package io.gitlab.chkypros.sf5btg.recipesapp.repositories;

import io.gitlab.chkypros.sf5btg.recipesapp.domain.UnitOfMeasure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UnitOfMeasureRepositoryIT {
    @Autowired
    private UnitOfMeasureRepository unitOfMeasureRepository;

    @Test
    public void findByName() {
        Optional<UnitOfMeasure> unitOfMeasure = unitOfMeasureRepository.findByName("Teaspoon");

        assertEquals("Teaspoon", unitOfMeasure.get().getName());
        assertEquals("tsp.", unitOfMeasure.get().getDescriptionSingular());
        assertEquals("tsp.", unitOfMeasure.get().getDescriptionPlural());
    }

    @Test
    public void findByDescriptionSingular() {
        Optional<UnitOfMeasure> unitOfMeasure = unitOfMeasureRepository.findByDescriptionSingular("tsp.");

        assertEquals("Teaspoon", unitOfMeasure.get().getName());
        assertEquals("tsp.", unitOfMeasure.get().getDescriptionSingular());
        assertEquals("tsp.", unitOfMeasure.get().getDescriptionPlural());
    }

    @Test
    public void findByDescriptionPlural() {
        Optional<UnitOfMeasure> unitOfMeasure = unitOfMeasureRepository.findByDescriptionPlural("tsp.");

        assertEquals("Teaspoon", unitOfMeasure.get().getName());
        assertEquals("tsp.", unitOfMeasure.get().getDescriptionSingular());
        assertEquals("tsp.", unitOfMeasure.get().getDescriptionPlural());
    }
}