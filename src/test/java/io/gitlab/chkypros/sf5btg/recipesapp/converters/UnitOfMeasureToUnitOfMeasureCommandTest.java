package io.gitlab.chkypros.sf5btg.recipesapp.converters;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.UnitOfMeasureCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.UnitOfMeasure;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class UnitOfMeasureToUnitOfMeasureCommandTest {
    private static final Long LONG_VALUE = 1L;
    private static final String NAME = "name";
    private static final String DESCRIPTION_SINGULAR = "description-singular";
    private static final String DESCRIPTION_PLURAL = "description-plural";

    private UnitOfMeasureToUnitOfMeasureCommand converter;

    @Before
    public void setUp() {
        converter = new UnitOfMeasureToUnitOfMeasureCommand();
    }

    @Test
    public void convert_nullParameter() {
        assertNull(converter.convert(null));
    }

    @Test
    public void convert_emptyObject() {
        assertNotNull(converter.convert(new UnitOfMeasure()));
    }

    @Test
    public void convert() {
        // given
        UnitOfMeasure unitOfMeasure = new UnitOfMeasure();
        unitOfMeasure.setId(LONG_VALUE);
        unitOfMeasure.setName(NAME);
        unitOfMeasure.setDescriptionSingular(DESCRIPTION_SINGULAR);
        unitOfMeasure.setDescriptionPlural(DESCRIPTION_PLURAL);

        // when
        UnitOfMeasureCommand command = converter.convert(unitOfMeasure);

        // then
        assertNotNull(command);
        assertEquals(LONG_VALUE, command.getId());
        assertEquals(NAME, command.getName());
        assertEquals(DESCRIPTION_SINGULAR, command.getDescriptionSingular());
        assertEquals(DESCRIPTION_PLURAL, command.getDescriptionPlural());
    }
}
