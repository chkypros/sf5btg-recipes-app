package io.gitlab.chkypros.sf5btg.recipesapp.controllers;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.IngredientCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.commands.RecipeCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.services.IngredientService;
import io.gitlab.chkypros.sf5btg.recipesapp.services.RecipeService;
import io.gitlab.chkypros.sf5btg.recipesapp.services.UnitOfMeasureService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class IngredientControllerTest {

    @Mock RecipeService recipeService;
    @Mock IngredientService ingredientService;
    @Mock UnitOfMeasureService unitOfMeasureService;

    IngredientController controller;

    MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        controller = new IngredientController(recipeService, ingredientService, unitOfMeasureService);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void listIngredients() throws Exception {
        // Given
        RecipeCommand recipeCommand = new RecipeCommand();
        doReturn(recipeCommand).when(recipeService).findCommandById(anyLong());

        // When
        mockMvc.perform(get("/recipe/1/ingredients"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/list"))
                .andExpect(model().attributeExists("recipe"));

        // Then
        verify(recipeService).findCommandById(anyLong());
    }

    @Test
    public void showIngredient() throws Exception {
        // Given
        IngredientCommand ingredientCommand = new IngredientCommand();
        doReturn(ingredientCommand).when(ingredientService).findCommandByRecipeIdAndIngredientId(anyLong(), anyLong());

        // When
        mockMvc.perform(get("/recipe/1/ingredient/2/show"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/show"))
                .andExpect(model().attributeExists("ingredient"));

        // Then
        verify(ingredientService).findCommandByRecipeIdAndIngredientId(anyLong(),anyLong());
    }

    @Test
    public void createNewIngredient() throws Exception {
        // Given
        RecipeCommand recipeCommand = new RecipeCommand();
        doReturn(recipeCommand).when(recipeService).findCommandById(anyLong());

        // When
        mockMvc.perform(get("/recipe/1/ingredient/new"))
            .andExpect(status().isOk())
            .andExpect(view().name("recipe/ingredient/ingredient-form"))
            .andExpect(model().attributeExists("ingredient"))
            .andExpect(model().attributeExists("uomList"));

        verify(recipeService).findCommandById(anyLong());
    }

    @Test
    public void updateIngredient() throws Exception {
        // Given
        RecipeCommand recipeCommand = new RecipeCommand();
        doReturn(recipeCommand).when(recipeService).findCommandById(anyLong());

        IngredientCommand ingredientCommand = new IngredientCommand();
        doReturn(ingredientCommand).when(ingredientService).findCommandByRecipeIdAndIngredientId(anyLong(), anyLong());

        // When
        mockMvc.perform(get("/recipe/1/ingredient/2/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/ingredient-form"))
                .andExpect(model().attributeExists("ingredient"))
                .andExpect(model().attributeExists("uomList"));

        // Then
        verify(ingredientService).findCommandByRecipeIdAndIngredientId(anyLong(), anyLong());
    }

    @Test
    public void saveOrUpdate() throws Exception {
        // Given
        IngredientCommand command = new IngredientCommand();
        command.setId(3L);
        command.setRecipeId(2L);

        doReturn(command).when(ingredientService).saveIngredientCommand(any());

        // When
        mockMvc.perform(post("/recipe/2/ingredient")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("id", "")
                        .param("description", "some string"))
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/recipe/2/ingredient/3/show"));

        // Then
        verify(ingredientService).saveIngredientCommand(any(IngredientCommand.class));
    }

    @Test
    public void deleteIngredient() throws Exception {
        // Given
        RecipeCommand recipeCommand = new RecipeCommand();
        doReturn(recipeCommand).when(recipeService).findCommandById(anyLong());

        // When
        mockMvc.perform(get("/recipe/2/ingredient/1/delete"))
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/recipe/2/ingredients"));

        // Then
        verify(ingredientService).deleteByRecipeIdAndIngredientId(anyLong(), anyLong());
    }
}