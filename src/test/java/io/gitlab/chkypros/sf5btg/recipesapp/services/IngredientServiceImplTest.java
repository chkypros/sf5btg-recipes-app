package io.gitlab.chkypros.sf5btg.recipesapp.services;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.IngredientCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.converters.IngredientCommandToIngredient;
import io.gitlab.chkypros.sf5btg.recipesapp.converters.IngredientToIngredientCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.converters.UnitOfMeasureCommandToUnitOfMeasure;
import io.gitlab.chkypros.sf5btg.recipesapp.converters.UnitOfMeasureToUnitOfMeasureCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Ingredient;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.Recipe;
import io.gitlab.chkypros.sf5btg.recipesapp.repositories.IngredientRepository;
import io.gitlab.chkypros.sf5btg.recipesapp.repositories.RecipeRepository;
import io.gitlab.chkypros.sf5btg.recipesapp.repositories.UnitOfMeasureRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class IngredientServiceImplTest {

    @Mock
    RecipeRepository recipeRepository;

    @Mock
    UnitOfMeasureRepository unitOfMeasureRepository;

    @Mock
    IngredientRepository ingredientRepository;

    private final IngredientToIngredientCommand ingredientToIngredientCommand;
    private final IngredientCommandToIngredient ingredientCommandToIngredient;

    private IngredientServiceImpl ingredientService;

    public IngredientServiceImplTest() {
        ingredientToIngredientCommand = new IngredientToIngredientCommand(new UnitOfMeasureToUnitOfMeasureCommand());
        ingredientCommandToIngredient = new IngredientCommandToIngredient(new UnitOfMeasureCommandToUnitOfMeasure());
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        ingredientService = new IngredientServiceImpl(ingredientToIngredientCommand, ingredientCommandToIngredient,
            recipeRepository, unitOfMeasureRepository, ingredientRepository);
    }

    @Test
    public void findCommandByRecipeIdAndIngredientId_HappyPath() {
        // Given
        Recipe recipe = new Recipe();
        recipe.setId(1L);

        Ingredient ingredient = new Ingredient();
        ingredient.setId(2L);

        ingredient.setRecipe(recipe);
        recipe.addIngredient(ingredient);

        Optional<Recipe> recipeOptional = Optional.of(recipe);
        doReturn(recipeOptional).when(recipeRepository).findById(anyLong());

        // When
        IngredientCommand ingredientCommand = ingredientService.findCommandByRecipeIdAndIngredientId(1L, 2L);

        // Then
        assertEquals(Long.valueOf(1L), ingredientCommand.getRecipeId());
        assertEquals(Long.valueOf(2L), ingredientCommand.getId());
        verify(recipeRepository).findById(anyLong());
    }

    @Test
    public void saveIngredientCommand() {
        // Given
        IngredientCommand command = new IngredientCommand();
        command.setId(3L);
        command.setRecipeId(2L);

        Optional<Recipe> recipeOptional = Optional.of(new Recipe());
        Recipe savedRecipe = new Recipe();
        Ingredient ingredient = new Ingredient();
        ingredient.setId(3L);
        savedRecipe.addIngredient(ingredient);

        doReturn(recipeOptional).when(recipeRepository).findById(anyLong());
        doReturn(savedRecipe).when(recipeRepository).save(any());

        // When
        IngredientCommand savedIngredientCommand = ingredientService.saveIngredientCommand(command);

        // Then
        assertEquals(Long.valueOf(3L), savedIngredientCommand.getId());
        verify(recipeRepository).findById(anyLong());
        verify(recipeRepository).save(any(Recipe.class));
    }
}