package io.gitlab.chkypros.sf5btg.recipesapp.services;

import io.gitlab.chkypros.sf5btg.recipesapp.commands.UnitOfMeasureCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.converters.UnitOfMeasureToUnitOfMeasureCommand;
import io.gitlab.chkypros.sf5btg.recipesapp.domain.UnitOfMeasure;
import io.gitlab.chkypros.sf5btg.recipesapp.repositories.UnitOfMeasureRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class UnitOfMeasureServiceImplTest {

    @Mock private UnitOfMeasureRepository unitOfMeasureRepository;

    private UnitOfMeasureToUnitOfMeasureCommand unitOfMeasureToUnitOfMeasureCommand = new UnitOfMeasureToUnitOfMeasureCommand();
    private UnitOfMeasureServiceImpl unitOfMeasureService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        unitOfMeasureService = new UnitOfMeasureServiceImpl(unitOfMeasureRepository, unitOfMeasureToUnitOfMeasureCommand);
    }

    @Test
    public void findAll() {
        // Given
        Set<UnitOfMeasure> unitsOfMeasure = new HashSet<>();
        UnitOfMeasure uom1 = new UnitOfMeasure();
        uom1.setId(1L);
        unitsOfMeasure.add(uom1);

        UnitOfMeasure uom2 = new UnitOfMeasure();
        uom2.setId(2L);
        unitsOfMeasure.add(uom2);

        doReturn(unitsOfMeasure).when(unitOfMeasureRepository).findAll();

        // When
        Set<UnitOfMeasureCommand> commands = unitOfMeasureService.findAll();

        // Then
        assertEquals(unitsOfMeasure.size(), commands.size());
        verify(unitOfMeasureRepository).findAll();
    }
}